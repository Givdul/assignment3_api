# Assignment 3

Entity Framework Code First workflow ASP.NET Core Web API in C# for movies, characters and franchises.
API made as required from the prerequisites in the assignment.

## Usage

* Clone to local directory

* Open solution in Visual Studio

* Run Migrations

* Run IIS Server

## Author
Ludvig Hansen

## License
[MIT](https://choosealicense.com/licenses/mit/)
