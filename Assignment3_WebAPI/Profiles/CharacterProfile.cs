﻿using Assignment3_WebAPI.Models;
using Assignment3_WebAPI.Models.DTO.Character;
using AutoMapper;
using System.Linq;

namespace Assignment3_WebAPI.Profiles
{
    public class CharacterProfile : Profile
    {
        public CharacterProfile()
        {
            CreateMap<Character, CharacterReadDTO>()
                .ForMember(cdto => cdto.Movies, opt => opt
                .MapFrom(c => c.Movies.Select(m => m.MovieId).ToList()))
                .ReverseMap();
            CreateMap<Character, CharacterCreateDTO>().ReverseMap();
            CreateMap<Character, CharacterEditDTO>().ReverseMap();
        }
    }
}
