﻿using Assignment3_WebAPI.Models;
using Assignment3_WebAPI.Models.DTO.Franchise;
using AutoMapper;
using System.Linq;

namespace Assignment3_WebAPI.Profiles
{
    public class FranchiseProfile : Profile
    {
        public FranchiseProfile()
        {
            CreateMap<Franchise, FranchiseReadDTO>()
                .ForMember(fdto => fdto.Movies, opt => opt
                .MapFrom(f => f.Movies.Select(m => m.MovieId).ToList()))
                .ForMember(Movies => Movies.Characters, opt => opt
                .MapFrom(Movies => Movies.Characters.Select(Movies => Movies.CharacterId).ToList()))
                .ReverseMap();
            CreateMap<Franchise, FranchiseCreateDTO>().ReverseMap();
            CreateMap<Franchise, FranchiseEditDTO>().ReverseMap();
        }
    }
}
