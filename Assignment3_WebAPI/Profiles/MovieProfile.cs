﻿using Assignment3_WebAPI.Models;
using Assignment3_WebAPI.Models.DTO.Movie;
using AutoMapper;
using System.Linq;

namespace Assignment3_WebAPI.Profiles
{
    public class MovieProfile : Profile
    {
        public MovieProfile()
        {
            CreateMap<Movie, MovieReadDTO>()
                .ForMember(mdto => mdto.Characters, opt=>opt
                .MapFrom(m=>m.Characters.Select(c=>c.CharacterId).ToList()))
                .ReverseMap();
            CreateMap<Movie, MovieCreateDTO>().ReverseMap();
            CreateMap<Movie, MovieEditDTO>().ReverseMap();
        }
    }
}
