﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Assignment3_WebAPI.Models;
using System.Net.Mime;
using Assignment3_WebAPI.Models.DTO.Movie;
using AutoMapper;

namespace Assignment3_WebAPI.Controllers
{
    [Route("api/v1/movies")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class MovieController : ControllerBase
    {
        private readonly MovieDbContext _context;
        private readonly IMapper _mapper;

        public MovieController(MovieDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        /// <summary>
        /// Get all Movies
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<MovieReadDTO>>> GetMovies()
        {
            return _mapper.Map<List<MovieReadDTO>>(await _context.Movies.Include(c=>c.Characters).ToListAsync());
        }

        /// <summary>
        /// Get specific Movie by Id
        /// </summary>
        /// <param name="id">MovieId</param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<MovieReadDTO>> GetMovie(int id)
        {
            Movie movie = await _context.Movies.FindAsync(id);

            if (movie == null)
            {
                return NotFound();
            }

            return _mapper.Map<MovieReadDTO>(movie);
        }

        /// <summary>
        /// Update specific Movie by Id
        /// </summary>
        /// <param name="id">MovieId</param>
        /// <param name="movie"></param>
        /// <returns></returns>
        /// <exception cref="DbUpdateConcurrencyException"></exception>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutMovie(int id, MovieEditDTO dtoMovie)
        {
            if (id != dtoMovie.MovieId)
            {
                return BadRequest();
            }
            Movie domainMovie = _mapper.Map<Movie>(dtoMovie);
            _context.Entry(domainMovie).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MovieExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }
        /// <summary>
        /// Create new Movie
        /// </summary>
        /// <param name="movie"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<Movie>> PostMovie(MovieCreateDTO dtoMovie)
        {
            Movie domainMovie = _mapper.Map<Movie>(dtoMovie);
            _context.Movies.Add(domainMovie);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetMovie", new { id = domainMovie.MovieId }, _mapper.Map<MovieReadDTO>(domainMovie));
        }

        /// <summary>
        /// Delete specific Movie by Id
        /// </summary>
        /// <param name="id">MovieId</param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteMovie(int id)
        {
            var movie = await _context.Movies.FindAsync(id);
            if (movie == null)
            {
                return NotFound();
            }

            _context.Movies.Remove(movie);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool MovieExists(int id)
        {
            return _context.Movies.Any(e => e.MovieId == id);
        }
        /// <summary>
        /// Update Characters in a specific Movie
        /// </summary>
        /// <param name="id">Movie Id</param>
        /// <param name="characters">List of Characters by Id</param>
        /// <returns></returns>
        /// <exception cref="DbUpdateConcurrencyException"></exception>
        [HttpPut("{id}/characters")]
        public async Task<IActionResult> UpdateMovieCharacters(int id, List<int> characters)
        {
            if (!MovieExists(id))
            {
                return NotFound();
            }

            Movie MovieUpdateCharacters = await _context.Movies
                .Include(c => c.Characters)
                .Where(c => c.MovieId == id)
                .FirstAsync();

            List<Character> characterList = new();
            foreach (int CharacterId in characters)
            {
                Character character = await _context.Characters.FindAsync(CharacterId);
                if (character == null)
                    return BadRequest("Character doesn't exist!");
                characterList.Add(character);
            }
            MovieUpdateCharacters.Characters = characterList;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }

            return NoContent();
        }
    }
}
