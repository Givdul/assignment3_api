﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Assignment3_WebAPI.Models;
using System.Net.Mime;
using AutoMapper;
using Assignment3_WebAPI.Models.DTO.Franchise;

namespace Assignment3_WebAPI.Controllers
{
    [Route("api/v1/franchises")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class FranchiseController : ControllerBase
    {
        private readonly MovieDbContext _context;
        public IMapper _mapper;

        public FranchiseController(MovieDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
    }

        /// <summary>
        /// Get all Franchises
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<FranchiseReadDTO>>> GetFranchises()
        {
            return _mapper.Map<List<FranchiseReadDTO>>(await _context.Franchises
                .Include(m => m.Movies)
                .Include(c => c.Characters)
                .ToListAsync());
        }

        /// <summary>
        /// Get specific Franchise By Id
        /// </summary>
        /// <param name="id">FranchiseId</param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<FranchiseReadDTO>> GetFranchise(int id)
        {
            Franchise franchise = await _context.Franchises.FindAsync(id);

            if (franchise == null)
            {
                return NotFound();
            }

            return _mapper.Map<FranchiseReadDTO>(franchise);
        }

        /// <summary>
        /// Update a specific Franchise by Id
        /// </summary>
        /// <param name="id">FranchiseId</param>
        /// <param name="franchise"></param>
        /// <returns></returns>
        /// <exception cref="DbUpdateConcurrencyException"></exception>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutFranchise(int id, FranchiseEditDTO dtoFranchise)
        {
            if (id != dtoFranchise.FranchiseId)
            {
                return BadRequest();
            }

            Franchise domainFranchise = _mapper.Map<Franchise>(dtoFranchise);
            _context.Entry(domainFranchise).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FranchiseExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        /// <summary>
        /// Create new Franchise
        /// </summary>
        /// <param name="franchise"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<Franchise>> PostFranchise(FranchiseCreateDTO dtoFranchise)
        {
            Franchise domainFranchise = _mapper.Map<Franchise>(dtoFranchise);
            _context.Franchises.Add(domainFranchise);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetFranchise", new { id = domainFranchise.FranchiseId }, _mapper.Map<FranchiseReadDTO>(domainFranchise));
        }

        /// <summary>
        /// Delete specific Franchise by Id
        /// </summary>
        /// <param name="id">FranchiseId</param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteFranchise(int id)
        {
            var franchise = await _context.Franchises.FindAsync(id);
            if (franchise == null)
            {
                return NotFound();
            }

            _context.Franchises.Remove(franchise);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool FranchiseExists(int id)
        {
            return _context.Franchises.Any(e => e.FranchiseId == id);
        }
        /// <summary>
        /// Update Movies in a specific Franchies
        /// </summary>
        /// <param name="id">Franchise Id</param>
        /// <param name="movies">List of Movies by Id</param>
        /// <returns></returns>
        /// <exception cref="DbUpdateConcurrencyException"></exception>
        [HttpPut("{id}/movies")]
        public async Task<IActionResult> UpdateFranchiseMovies(int id, List<int> movies)
        {
            if (!FranchiseExists(id))
            {
                return NotFound();
            }

            Franchise franchiseUpdateMovies = await _context.Franchises
                .Include(m => m.Movies)
                .Where(m => m.FranchiseId == id)
                .FirstAsync();

            List<Movie> movieList = new();
            foreach (int movieId in movies)
            {
                Movie movie = await _context.Movies.FindAsync(movieId);
                if (movie == null)
                    return BadRequest("Movie doesn't exist!");
                movieList.Add(movie);
            }
            franchiseUpdateMovies.Movies = movieList;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }

            return NoContent();
        }
    }
}
