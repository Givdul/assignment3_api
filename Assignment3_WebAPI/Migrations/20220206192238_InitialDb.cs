﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Assignment3_WebAPI.Migrations
{
    public partial class InitialDb : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Franchises",
                columns: table => new
                {
                    FranchiseId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Franchises", x => x.FranchiseId);
                });

            migrationBuilder.CreateTable(
                name: "Characters",
                columns: table => new
                {
                    CharacterId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    Alias = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Gender = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    PictureURL = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    FranchiseId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Characters", x => x.CharacterId);
                    table.ForeignKey(
                        name: "FK_Characters_Franchises_FranchiseId",
                        column: x => x.FranchiseId,
                        principalTable: "Franchises",
                        principalColumn: "FranchiseId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Movies",
                columns: table => new
                {
                    MovieId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Title = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    Genre = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ReleaseYear = table.Column<int>(type: "int", nullable: false),
                    Director = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    PictureURL = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    TrailerURL = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    FranchiseId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Movies", x => x.MovieId);
                    table.ForeignKey(
                        name: "FK_Movies_Franchises_FranchiseId",
                        column: x => x.FranchiseId,
                        principalTable: "Franchises",
                        principalColumn: "FranchiseId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "MovieCharacters",
                columns: table => new
                {
                    MovieId = table.Column<int>(type: "int", nullable: false),
                    CharacterId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MovieCharacters", x => new { x.MovieId, x.CharacterId });
                    table.ForeignKey(
                        name: "FK_MovieCharacters_Characters_CharacterId",
                        column: x => x.CharacterId,
                        principalTable: "Characters",
                        principalColumn: "CharacterId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_MovieCharacters_Movies_MovieId",
                        column: x => x.MovieId,
                        principalTable: "Movies",
                        principalColumn: "MovieId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Characters",
                columns: new[] { "CharacterId", "Alias", "FranchiseId", "Gender", "Name", "PictureURL" },
                values: new object[,]
                {
                    { 1, "Blade Runner", null, "Male", "Rick Deckard", "https://static.wikia.nocookie.net/bladerunner/images/d/de/Deckard.jpg/revision/latest/scale-to-width-down/350?cb=20190318190453" },
                    { 2, "Joe", null, "Male Replicant", "K", "https://static.wikia.nocookie.net/bladerunner/images/9/90/K.jpg/revision/latest/scale-to-width-down/350?cb=20190519045609" },
                    { 3, null, null, "Female Replicant", "Rachael", "https://static.wikia.nocookie.net/bladerunner/images/2/2e/Rachael.jpg/revision/latest/scale-to-width-down/350?cb=20190311031039" },
                    { 4, null, null, "Female", "Ana Stelline", "https://static.wikia.nocookie.net/bladerunner/images/2/25/Ana.jpg/revision/latest/scale-to-width-down/333?cb=20171213202803" },
                    { 5, null, null, "Male", "Benoit Blanc", "https://xl.movieposterdb.com/21_07/2019/8946378/xl_8946378_09cdd33c.jpg?v=2021-10-21%2013:50:25" },
                    { 6, null, null, "Male", "Harlan Thrombey", "https://xl.movieposterdb.com/20_11/2019/8946378/xl_8946378_41403b04.jpg?v=2021-10-21%2013:50:25" },
                    { 7, null, null, "Female", "Marta Cabrera", "https://xl.movieposterdb.com/20_11/2019/8946378/xl_8946378_fbb476ee.jpg?v=2021-10-21%2013:50:25" }
                });

            migrationBuilder.InsertData(
                table: "Franchises",
                columns: new[] { "FranchiseId", "Description", "Name" },
                values: new object[,]
                {
                    { 1, "Blade Runner is an American neo-noir science fiction media franchise originating from the 1968 novel Do Androids Dream of Electric Sheep? by Philip K. Dick, about the character of Rick Deckard.", "Blade Runner" },
                    { 2, "Knives Out is a 2019 American mystery film written and directed by Rian Johnson and produced by Johnson and Ram Bergman. It follows a master detective investigating the death of the patriarch of a wealthy, dysfunctional family.", "Knives Out" }
                });

            migrationBuilder.InsertData(
                table: "Movies",
                columns: new[] { "MovieId", "Director", "FranchiseId", "Genre", "PictureURL", "ReleaseYear", "Title", "TrailerURL" },
                values: new object[] { 1, "Ridley Scott", 1, "Science Fiction, Action, Drama", "https://xl.movieposterdb.com/14_04/1982/83658/xl_83658_40e58a2e.jpg", 1982, "Blade Runner", "https://youtu.be/iYhJ7Mf2Oxs" });

            migrationBuilder.InsertData(
                table: "Movies",
                columns: new[] { "MovieId", "Director", "FranchiseId", "Genre", "PictureURL", "ReleaseYear", "Title", "TrailerURL" },
                values: new object[] { 2, "Denis Villeneuve", 1, "Science Fiction, Action, Drama, Mystery", "https://xl.movieposterdb.com/19_12/2017/1856101/xl_1856101_2c68bb27.jpg", 2017, "Blade Runner 2049", "https://youtu.be/gCcx85zbxz4" });

            migrationBuilder.InsertData(
                table: "Movies",
                columns: new[] { "MovieId", "Director", "FranchiseId", "Genre", "PictureURL", "ReleaseYear", "Title", "TrailerURL" },
                values: new object[] { 3, "Rian Johnson", 2, "Comedy, Crime, Drama", "https://xl.movieposterdb.com/21_08/2019/8946378/xl_8946378_4a1d9009.jpg", 2019, "Knives Out", "https://youtu.be/sL-9Khv7wa4" });

            migrationBuilder.InsertData(
                table: "MovieCharacters",
                columns: new[] { "CharacterId", "MovieId" },
                values: new object[,]
                {
                    { 1, 1 },
                    { 3, 1 },
                    { 1, 2 },
                    { 2, 2 },
                    { 3, 2 },
                    { 4, 2 },
                    { 5, 3 },
                    { 6, 3 },
                    { 7, 3 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Characters_FranchiseId",
                table: "Characters",
                column: "FranchiseId");

            migrationBuilder.CreateIndex(
                name: "IX_MovieCharacters_CharacterId",
                table: "MovieCharacters",
                column: "CharacterId");

            migrationBuilder.CreateIndex(
                name: "IX_Movies_FranchiseId",
                table: "Movies",
                column: "FranchiseId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "MovieCharacters");

            migrationBuilder.DropTable(
                name: "Characters");

            migrationBuilder.DropTable(
                name: "Movies");

            migrationBuilder.DropTable(
                name: "Franchises");
        }
    }
}
