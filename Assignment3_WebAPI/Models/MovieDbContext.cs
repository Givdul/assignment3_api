﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;

namespace Assignment3_WebAPI.Models
{
    public class MovieDbContext : DbContext
    {
        //Tables
        public DbSet<Character> Characters { get; set; }
        public DbSet<Movie> Movies { get; set; }
        public DbSet<Franchise> Franchises { get; set; }

        public MovieDbContext(DbContextOptions options) : base(options)
        {
        }
        /// <summary>
        /// Send predefined data to the database when its created
        /// </summary>
        /// <param name="modelBuilder"></param>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //Seed data
            //Franchises
            modelBuilder.Entity<Franchise>().HasData(new Franchise() { FranchiseId = 1, Name = "Blade Runner", Description = "Blade Runner is an American neo-noir science fiction media franchise originating from the 1968 novel Do Androids Dream of Electric Sheep? by Philip K. Dick, about the character of Rick Deckard." });
            modelBuilder.Entity<Franchise>().HasData(new Franchise() { FranchiseId = 2, Name = "Knives Out", Description = "Knives Out is a 2019 American mystery film written and directed by Rian Johnson and produced by Johnson and Ram Bergman. It follows a master detective investigating the death of the patriarch of a wealthy, dysfunctional family." });
            //Movies
            modelBuilder.Entity<Movie>().HasData(new Movie() { MovieId = 1, Title = "Blade Runner", Genre = "Science Fiction, Action, Drama", ReleaseYear = 1982, Director = "Ridley Scott", PictureURL = "https://xl.movieposterdb.com/14_04/1982/83658/xl_83658_40e58a2e.jpg", TrailerURL = "https://youtu.be/iYhJ7Mf2Oxs", FranchiseId = 1 });
            modelBuilder.Entity<Movie>().HasData(new Movie() { MovieId = 2, Title = "Blade Runner 2049", Genre = "Science Fiction, Action, Drama, Mystery", ReleaseYear = 2017, Director = "Denis Villeneuve", PictureURL = "https://xl.movieposterdb.com/19_12/2017/1856101/xl_1856101_2c68bb27.jpg", TrailerURL = "https://youtu.be/gCcx85zbxz4", FranchiseId = 1 });
            modelBuilder.Entity<Movie>().HasData(new Movie() { MovieId = 3, Title = "Knives Out", Genre = "Comedy, Crime, Drama", ReleaseYear = 2019, Director = "Rian Johnson", PictureURL = "https://xl.movieposterdb.com/21_08/2019/8946378/xl_8946378_4a1d9009.jpg", TrailerURL = "https://youtu.be/sL-9Khv7wa4", FranchiseId = 2 });
            //Characters
            //Blade Runner Franchise Characters
            modelBuilder.Entity<Character>().HasData(new Character() { CharacterId = 1, Name = "Rick Deckard", Alias = "Blade Runner", Gender = "Male", PictureURL = "https://static.wikia.nocookie.net/bladerunner/images/d/de/Deckard.jpg/revision/latest/scale-to-width-down/350?cb=20190318190453" });
            modelBuilder.Entity<Character>().HasData(new Character() { CharacterId = 2, Name = "K", Alias = "Joe", Gender = "Male Replicant", PictureURL = "https://static.wikia.nocookie.net/bladerunner/images/9/90/K.jpg/revision/latest/scale-to-width-down/350?cb=20190519045609" });
            modelBuilder.Entity<Character>().HasData(new Character() { CharacterId = 3, Name = "Rachael", Gender = "Female Replicant", PictureURL = "https://static.wikia.nocookie.net/bladerunner/images/2/2e/Rachael.jpg/revision/latest/scale-to-width-down/350?cb=20190311031039" });
            modelBuilder.Entity<Character>().HasData(new Character() { CharacterId = 4, Name = "Ana Stelline", Gender = "Female", PictureURL = "https://static.wikia.nocookie.net/bladerunner/images/2/25/Ana.jpg/revision/latest/scale-to-width-down/333?cb=20171213202803" });
            //Knives Out Franchise Characters
            modelBuilder.Entity<Character>().HasData(new Character() { CharacterId = 5, Name = "Benoit Blanc", Gender = "Male", PictureURL = "https://xl.movieposterdb.com/21_07/2019/8946378/xl_8946378_09cdd33c.jpg?v=2021-10-21%2013:50:25" });
            modelBuilder.Entity<Character>().HasData(new Character() { CharacterId = 6, Name = "Harlan Thrombey", Gender = "Male", PictureURL = "https://xl.movieposterdb.com/20_11/2019/8946378/xl_8946378_41403b04.jpg?v=2021-10-21%2013:50:25" });
            modelBuilder.Entity<Character>().HasData(new Character() { CharacterId = 7, Name = "Marta Cabrera", Gender = "Female", PictureURL = "https://xl.movieposterdb.com/20_11/2019/8946378/xl_8946378_fbb476ee.jpg?v=2021-10-21%2013:50:25" });
            //m2m seeding movies and characters
            modelBuilder.Entity<Movie>()
                .HasMany(p => p.Characters)
                .WithMany(m => m.Movies)
                .UsingEntity<Dictionary<string, object>>(
                    "MovieCharacters",
                    r => r.HasOne<Character>().WithMany().HasForeignKey("CharacterId"),
                    l => l.HasOne<Movie>().WithMany().HasForeignKey("MovieId"),
                    je =>
                    {
                        je.HasKey("MovieId", "CharacterId");
                        je.HasData(
                            //Blade Runner
                            new { MovieId = 1, CharacterId = 1 },
                            new { MovieId = 1, CharacterId = 3 },
                            //Blade Runner 2049
                            new { MovieId = 2, CharacterId = 1 },
                            new { MovieId = 2, CharacterId = 2 },
                            new { MovieId = 2, CharacterId = 3 },
                            new { MovieId = 2, CharacterId = 4 },
                            //Knives Out
                            new { MovieId = 3, CharacterId = 5 },
                            new { MovieId = 3, CharacterId = 6 },
                            new { MovieId = 3, CharacterId = 7 }
                            );
                    });
        }
    }
}
