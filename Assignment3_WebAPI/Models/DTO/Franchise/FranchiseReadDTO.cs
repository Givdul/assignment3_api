﻿using System.Collections.Generic;

namespace Assignment3_WebAPI.Models.DTO.Franchise
{
    public class FranchiseReadDTO
    {
        public int FranchiseId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public List<int> Movies { get; set; }
        public List<int> Characters { get; set; }
    }
}
