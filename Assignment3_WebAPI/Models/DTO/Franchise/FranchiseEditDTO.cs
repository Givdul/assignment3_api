﻿namespace Assignment3_WebAPI.Models.DTO.Franchise
{
    public class FranchiseEditDTO
    {
        public int FranchiseId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
