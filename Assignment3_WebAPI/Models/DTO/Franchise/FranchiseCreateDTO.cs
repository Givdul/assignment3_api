﻿namespace Assignment3_WebAPI.Models.DTO.Franchise
{
    public class FranchiseCreateDTO
    {
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
