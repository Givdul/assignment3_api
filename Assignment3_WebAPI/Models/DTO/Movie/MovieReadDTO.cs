﻿using System.Collections.Generic;

namespace Assignment3_WebAPI.Models.DTO.Movie
{
    public class MovieReadDTO
    {
        public int MovieId { get; set; }
        public string Title { get; set; }
        public string Genre { get; set; }
        public int ReleaseYear { get; set; }
        public string Director { get; set; }
        public string PictureURL { get; set; }
        public string TrailerURL { get; set; }
        public int FranchiseId { get; set; }
        public List<int> Characters { get; set; }
    }
}
