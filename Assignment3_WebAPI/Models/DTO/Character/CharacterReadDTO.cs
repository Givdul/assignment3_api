﻿using System.Collections.Generic;

namespace Assignment3_WebAPI.Models.DTO.Character
{
    public class CharacterReadDTO
    {
        public int CharacterId { get; set; }
        public string Name { get; set; }
        public string Alias { get; set; }
        public string Gender { get; set; }
        public string PictureURL { get; set; }
        public List<int> Movies { get; set; }
    }
}
