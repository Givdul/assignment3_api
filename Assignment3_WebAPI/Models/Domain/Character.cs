﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Assignment3_WebAPI.Models
{
    public class Character
    {
        //PK
        public int CharacterId { get; set; }
        //Fields
        [Required]
        [MaxLength(50)]
        public string Name { get; set; }
        public string Alias { get; set; }
        [MaxLength(50)]
        public string Gender { get; set; }
        public string PictureURL { get; set; }
        //FK
        public ICollection<Movie> Movies { get; set; }
    }
}