﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Assignment3_WebAPI.Models
{
    public class Franchise
    {
        //PK
        public int FranchiseId { get; set; }
        //Fields
        [Required]
        public string Name { get; set; }
        public string Description { get; set; }
        //FK
        public ICollection<Movie>? Movies { get; set; }
        public ICollection<Character>? Characters { get; set; }
    }
}