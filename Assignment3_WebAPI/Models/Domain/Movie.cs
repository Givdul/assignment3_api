﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Assignment3_WebAPI.Models
{
    public class Movie
    {
        //PK
        public int MovieId { get; set; }
        //Fields
        [Required]
        [MaxLength(100)]
        public string Title { get; set; }
        public string Genre { get; set; }
        public int ReleaseYear { get; set; }
        public string Director { get; set; }
        public string PictureURL { get; set; }
        public string TrailerURL { get; set; }
        //FK
        public int FranchiseId { get; set; }
        public Franchise Franchise { get; set; }
        public ICollection<Character>? Characters { get; set; }
    }
}